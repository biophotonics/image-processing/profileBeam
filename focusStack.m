%% This function loads multiple beam profiles obtained through focus and calculates some parameters
%
%

%% Setup:
setup.count = 5;            % Number of measurements
setup.wavelength = 780;     % set wavelength in nm
setup.f=100;                % focal length of the lens in mm

%% Load file

%% Run
for i = 1:setup.count
[discard.cColum,discard.cRow,sx(i),sy(i),angle(i)]=profileBeam();
if angle(i)>1 % selection of axis is not fixed, swap them around at large angles
    discard.sx=sx(i);
    sx(i)=sy(i);
    sy(i)=discard.sx;
end
z(i) = input('please type Z-position');
end


%% Analysis
% Fit a 2nd order polynom
p.x = polyfit(z,sx,2); 
p.y = polyfit(z,sy,2);
% plot the fit
fit.z = linspace(min(z),max(z),100); % create axis and draw 
fit.x = polyval(p.x,fit.z);
fit.y = polyval(p.y,fit.z);

%% find foci positions
[discard.search,discard.Xpos] = min(fit.x);
Xpos = fit.z(discard.Xpos);
[discard.search,discard.Ypos] = min(fit.y);
Ypos = fit.z(discard.Ypos);

% Calculate parameters
Astig=abs(Xpos-Ypos);
DivX=min(fit.x)/setup.f;
DivY=min(fit.y)/setup.f;

%% Plot focus curve
plot(z,sx,z,sy,fit.z,fit.x,fit.z,fit.y)
%% CALCULATE m^2, BUGGY!!
% %www-ferp.ucsd.edu/LASERLAB/TUTOR/m2.html
% %find Rayleight positions :   Rayleight Range: Zr=z0*1.414 (Z0=focus)
% [discard.search,discard.Xray]= min(abs(fit.x - min(fit.x)*1.414));
% Xray = fit.z(discard.Xray);
% Xraylength = abs(Xpos-Xray); % rayleight range (distance: focus - focus*1.414) %seems about right
% X0 = 1000*2*sqrt(Xraylength*1E-3*setup.wavelength*1E-9/pi()); %!!! imbedded Gaussian Diameter in mm, seems to be very large
% M2 = (min(fit.x)/X0)^2; %!!! M2 should be more than 1

% more input ans Standarts: https://en.wikipedia.org/wiki/Laser_beam_quality


