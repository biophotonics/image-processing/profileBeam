# README #

### What is this repository for? ###

profileBeam takes a gray scale head on image of a laser beam and fits a 2D elliptical Gaussian to it.

### How do I get set up? ###

You need MATLAB to run this code.

You can either load your image into a matlab dataarray beforehand and then call profileBeam(dataarray), or you have saved the image in a data directory of the structure "data/yyyy-mm/yyyy-mm-dd/" and then call profileBeam without any arguments.

### Contribution guidelines ###

This software is in productive use. When coding, make sure you don't brake the existing code (or, even better, fix the wreck), comment in the files what you changed, and commit.

### Who do I talk to? ###

This repository is part of the DTU Fotonik "biophotonics" team.
The main responsible is Dominik Marti: domar@fotonik.dtu.dk