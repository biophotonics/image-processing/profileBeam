% Hello and welcome back. This routine tries to fit an image, taken for
% example with the pixelink camera, with a gaussian. It doesn't take any
% input parameters, but will return the center, the 1/e-radius (both in mm)
% and the tilt of the beam.

function [cCol, cRow, sx, sy, angle] = profileBeam(data)

    if nargin == 0

        close all;
%%
        % Load the image. Prompt the user with a dialog:
        fileExtension = {'*.tif;*.tiff;*.png;*.jpg;*.jpeg'};
        [fileName, directoryPath] = uigetfile(fileExtension);

        if isequal(fileName,0)
           return;
        end

        % load the file and convert data
        data=imread([directoryPath, '/', fileName]);
        
        % convert rgb image to grayscale
        if size(data,3) > 1 
           data=rgb2gray(squeeze(data(:,:,1:3)));
        end
        
        data=double(data);
        data=data/max(max(data));

        % Show the file to roi it
        screenSize = get(0,'ScreenSize');

        figure('Position', [10 55 screenSize(3)-20 screenSize(4)-125]);
        colormap gray;
        imagesc(data);
        axis image;

        title('Please roi your image');
        k = waitforbuttonpress;
        if k == 1; close all; return; end
        point1 = get(gca,'CurrentPoint');    % button down detected
        rbbox;                               % return figure units
        point2 = get(gca,'CurrentPoint');    % button up detected
        title('Calculating ...'); drawnow;

        roiCorners(:,1) = sort(round([point1(1,1) point2(1,1)]));              % extract x and y
        roiCorners(:,2) = sort(round([point1(1,2) point2(1,2)]));

        %set(figid,'Position',[427 290 512 384]);

        data=data(max(roiCorners(1,2),1):min(roiCorners(2,2),size(data,2)),max(roiCorners(1,1),1):min(roiCorners(2,1),size(data,2)));

        clear('screenSize', 'roiCorners', 'k', 'point1', 'point2');

    end

    % Now let's subtract the background, i.e. the mean of the edges
    background_mean = (mean(data(1,:)) + mean(data(end,:)) + mean(data(:,1)) + mean(data(:,end)))/4;
    data = data-background_mean;
    clear('background_mean')

    data=data/max(data(:));
    
    % The pixelink has      15.5 µm rowheight, 16 um/pixel columnwidth (with taper)    
    % The pco.1600 has      7.4  µm/pixel
    % The sensicam has      6.45 µm/pixel
    % The Typhoon has       6.25 µm/pixel
    % Torlabs DCC1545M has  5.2 µm/pixel
    % The Flea3 has         5.3 µm/pixel
    rowPitch=5.2; % µm/pixel
    colPitch=5.2; % µm/pixel

    numberOfRows=size(data,1);
    numberOfCols=size(data,2);
    rowVector=(1:numberOfRows)*rowPitch/1000; % in mm
    colVector=(1:numberOfCols)*colPitch/1000; % in mm

    [colIndices, rowIndices] = meshgrid(colVector, rowVector);
    rowColIndices = cat(2, rowIndices(:), colIndices(:));
    clear('rowIndices', 'colIndices')
    
    % Calculate the center of mass
    sumOverRows = sum(data,2)';
    sumOverCols = sum(data,1);

    sumOverRows = sumOverRows.*(sumOverRows>0);
    sumOverCols = sumOverCols.*(sumOverCols>0);

    cRow = sum(sumOverRows.*(rowVector))/sum(sumOverRows);
    cCol = sum(sumOverCols.*(colVector))/sum(sumOverCols);

    sy = sqrt(sum(sumOverRows.*(abs((rowVector)-cRow).^2))/sum(sumOverRows)*2);
    sx = sqrt(sum(sumOverCols.*(abs((colVector)-cCol).^2))/sum(sumOverCols)*2);

    clear('sumOverRows','sumOverCols')
    
    if nargin == 0

        disp(['Start parameters (1/e): ',...
            'cRow=', num2str(cRow), 'mm, ',...
            'cColumn=', num2str(cCol), 'mm, ',...
            'sx=', num2str(sx), 'mm, ',...
            'sy=', num2str(sy), 'mm, ',...
            'Peak=1, Angle=0']);
    end
    
    % Ok, let's try to fit the stuff
    options = optimset('TolFun',1e-12,'Jacobian','on');
    fit = lsqcurvefit(@gaussian2D, [cRow, cCol, sy, sx, 1, 0], rowColIndices,data(:), ...
        [0, 0, 0, 0, 0, -pi/2], [rowVector(end), colVector(end), 10, 10, 1.5, pi/2], options);
    fit_image = reshape(gaussian2D(fit,rowColIndices), numberOfRows, numberOfCols);

    cCol = fit(1); % in mm
    cRow = fit(2); % in mm
    
    sy = fit(3); % in mm
    sx = fit(4); % in mm

    % The angle from the fit is measured in clockwise direction against the
    % positive x-axis of a standard graph
    
    angle = fit(6); % in radian    
    
    disp(['End parameters (1/e): ', ...
        'cRow=', num2str(cRow), 'mm, cColumn=', num2str(cCol), 'mm, ', ...
        'sx=', num2str(sx), 'mm, sy=', num2str(sy), 'mm, ', ...
        'Peak=', num2str(fit(5)), ', Angle =', num2str(angle/pi), 'pi']);

    % Now we try to extract the profile along the sx and sy direction
    [rotatedRowIndicesX, rotatedColIndicesX, xVectorSx] = getRotatedIndices(cCol,cRow,angle,rowVector,colVector);
    [rotatedRowIndicesY, rotatedColIndicesY, xVectorSy] = getRotatedIndices(cCol,cRow,angle-pi/2,rowVector,colVector);

    %xVectorSx = sign(colVector(rotatedColIndicesX)-cCol) .* sqrt((colVector(rotatedColIndicesX)-cCol).^2 + (rowVector(rotatedRowIndicesX)-cRow).^2);
    %xVectorSy = -sign(rowVector(rotatedRowIndicesY)-cRow) .* sqrt((colVector(rotatedColIndicesY)-cCol).^2 + (rowVector(rotatedRowIndicesY)-cRow).^2);

    % Plot the two images
    subplot(2, 6, [1, 2])
    colormap gray;
    imagesc((colVector-cCol),(rowVector-cRow),data);
    axis image;
    title('Beam image');
    xlabel('[mm]')
    ylabel('[mm]')
    hold on
    plot(colVector(rotatedColIndicesX)-cCol,rowVector(rotatedRowIndicesX)-cRow,'c')
    plot(colVector(rotatedColIndicesX(1))-cCol,rowVector(rotatedRowIndicesX(1))-cRow,'oc')
    plot(colVector(rotatedColIndicesY)-cCol,rowVector(rotatedRowIndicesY)-cRow,'y')
    plot(colVector(rotatedColIndicesY(1))-cCol,rowVector(rotatedRowIndicesY(1))-cRow,'oy')

    subplot(2, 6, [5, 6])
    colormap gray;
    imagesc((colVector-cCol),(rowVector-cRow),fit_image)
    axis image;
    title('Fitted image');
    xlabel('[mm]')
    ylabel('[mm]')
    hold on
    plot(colVector(rotatedColIndicesX)-cCol,rowVector(rotatedRowIndicesX)-cRow,'c')
    plot(colVector(rotatedColIndicesX(1))-cCol,rowVector(rotatedRowIndicesX(1))-cRow,'oc')
    plot(colVector(rotatedColIndicesY)-cCol,rowVector(rotatedRowIndicesY)-cRow,'y')
    plot(colVector(rotatedColIndicesY(1))-cCol,rowVector(rotatedRowIndicesY(1))-cRow,'oy')

    subplot(2, 6, [3, 4])
    colormap gray;
    imagesc((colVector-cCol),(rowVector-cRow),data-fit_image)
    axis image;
    title('Difference image');
    xlabel('[mm]')
    ylabel('[mm]')
    hold on
    plot(colVector(rotatedColIndicesX)-cCol,rowVector(rotatedRowIndicesX)-cRow,'c')
    plot(colVector(rotatedColIndicesX(1))-cCol,rowVector(rotatedRowIndicesX(1))-cRow,'oc')
    plot(colVector(rotatedColIndicesY)-cCol,rowVector(rotatedRowIndicesY)-cRow,'y')
    plot(colVector(rotatedColIndicesY(1))-cCol,rowVector(rotatedRowIndicesY(1))-cRow,'oy')


    XLim = ceil((max([abs(xVectorSx), abs(xVectorSy)]))*4)/4;
    XLim = [-XLim, XLim];
    YLim = [-0.1 1.1];

    subplot(2, 6, [7, 8, 9])
    plot(xVectorSx,data(rotatedRowIndicesX + (rotatedColIndicesX-1)*numberOfRows),'b');
    hold on;
    plot(xVectorSx,fit_image(rotatedRowIndicesX + (rotatedColIndicesX-1)*numberOfRows),'r');
    evec=ones(size(xVectorSx))/exp(1)*fit(5);
    evec2=evec/exp(1);
    plot(xVectorSx,evec,'y');
    plot(xVectorSx,evec2,'y');
    %axis equal;
    set(gca,'XLim',XLim,'YLim',YLim)
    title(['x profile (cyan), 1/e radius: ', num2str(sx), 'mm']);
    xlabel('[mm]')
    ylabel('Intensity [a.u.]')

    subplot(2, 6, [10, 11, 12])
    plot(xVectorSy,data(rotatedRowIndicesY + (rotatedColIndicesY-1)*numberOfRows),'b');
    hold on;
    plot(xVectorSy,fit_image(rotatedRowIndicesY + (rotatedColIndicesY-1)*numberOfRows),'r');
    evec=ones(size(xVectorSy))/exp(1)*fit(5);
    evec2=evec/exp(1);
    plot(xVectorSy,evec2,'y');
    plot(xVectorSy,evec,'y');
    %axis equal;
    set(gca,'XLim',XLim,'YLim',[-0.1 1.1])
    title(['y profile (yellow), 1/e radius: ', num2str(sy), 'mm']);
    xlabel('[mm]')
    ylabel('Intensity [a.u.]')

        
return;

function [P, J] = gaussian2D(params, rowColIndices)
    % This is the helper function and describes a 2D-gauss
    
    cCol=params(1);
    cRow=params(2);
    sy=params(3);
    sx=params(4);
    peak=params(5);
    angle=params(6);
    
    sinAngle = sin(angle);
    cosAngle = cos(angle);
    
    x = (rowColIndices(:,1) - cRow)*sinAngle + (rowColIndices(:,2) - cCol)*cosAngle;
    y = (rowColIndices(:,1) - cRow)*cosAngle - (rowColIndices(:,2) - cCol)*sinAngle;
    
    
    % The angle is measured in clockwise direction from the positive x-axis
    % If angle == 0, sx measures the number of columns and sy the number of
    % rows

    P = peak * exp( - x.^2./sx^2 - y.^2./sy^2 );
    
    % Since, sometimes, the user oversaturates the camera, we deal with it
    % here:
    
    P(P > 1) = 1;
    
    if nargout > 1
        % Calculate the Jacobian
        
        J = zeros(length(x),6);
        
        J(:,1) = P .* ( ...
            + 2 * x ./ sx^2 * cosAngle ...
            - 2 * y ./ sy^2 * sinAngle ...
            );
        
        J(:,2) =  P .* ( ...
            + 2 * x ./ sx^2 * sinAngle ...
            + 2 * y ./ sy^2 * cosAngle ...
            );
        
        J(:,3) = P .* ( ...
            2 * y.^2 ./ sy^3 ...
            );

        J(:,4) = P .* ( ...
            2 * x.^2 ./ sx^3 ...
            );
        
        J(:,5) = P/peak;
        
        J(:,6) = P .* ( ...
            - 2 * x ./ sx^2 .* ...
            ( + (rowColIndices(:,1) - cRow)*cosAngle - (rowColIndices(:,2) - cCol)*sinAngle ) ...
            - 2 * y ./ sy^2 .* ...
            ( - (rowColIndices(:,1) - cRow)*sinAngle - (rowColIndices(:,2) - cCol)*cosAngle ) ...
            );
        
    end

return;

function [rotatedRowIndices, rotatedColIndices, xVector] = getRotatedIndices(cCol, cRow, angle, rowVector, colVector)

    % The outcome here depends on the angle of the beam and the size of the
    % image.
    % For now, you have to make the roi quadratic, with the beam in the centre.

    % Calculate the intersection of the x-axis with the edge of the image
    
    if abs(mod(angle,pi)) < 0.001
        
        rValueInterColMin = cRow;
        rValueInterColMax = cRow;
        
        cValueInterRowMin = colVector(1);
        cValueInterRowMax = colVector(end);
        
    elseif abs(mod(angle + pi/2,pi)) < 0.001
        
        rValueInterColMin = rowVector(1);
        rValueInterColMax = rowVector(end);
        
        cValueInterRowMin = cCol;
        cValueInterRowMax = cCol;

    elseif (angle > 0 && angle < pi/2) || (angle < -pi/2 && angle > -pi)
        
        % Both indices run in positive direction
        
        rValueInterColMin = (colVector(1)-cCol)*tan(angle)+cRow;
        rValueInterColMax = (colVector(end)-cCol)*tan(angle)+cRow;
        
        cValueInterRowMin = (rowVector(1)-cRow)*cot(angle)+cCol;
        cValueInterRowMax = (rowVector(end)-cRow)*cot(angle)+cCol;

    elseif (angle > pi/2 && angle < pi) || (angle < 0 && angle > -pi/2)
        
        rValueInterColMin = (colVector(1)-cCol)*tan(angle)+cRow;
        rValueInterColMax = (colVector(end)-cCol)*tan(angle)+cRow;
        
        % Max and Min are changed here, since col indices have to run in
        % negative direction
        cValueInterRowMax = (rowVector(1)-cRow)*cot(angle)+cCol;
        cValueInterRowMin = (rowVector(end)-cRow)*cot(angle)+cCol;

    end
    
    [~, rowStart] = min(abs(rowVector - rValueInterColMin));
    [~, rowEnd] = min(abs(rowVector - rValueInterColMax));
        
    [~, colStart] = min(abs(colVector - cValueInterRowMin));
    [~, colEnd] = min(abs(colVector - cValueInterRowMax));

    numberOfElements = max(abs(rowStart-rowEnd)+1, abs(colStart-colEnd)+1);
    
    rotatedRowIndices = round(linspace(rowStart, rowEnd, numberOfElements));
    rotatedColIndices = round(linspace(colStart, colEnd, numberOfElements));
    
    rValueInterColMin = rowVector(rowStart);
    rValueInterColMax = rowVector(rowEnd);
    cValueInterRowMin = colVector(colStart);
    cValueInterRowMax = colVector(colEnd);
    
    xVector = linspace(0, sqrt((rValueInterColMax - rValueInterColMin)^2 ...
        + (cValueInterRowMax - cValueInterRowMin)^2), numberOfElements) ...
        - sqrt((cRow - rValueInterColMin)^2+(cCol - cValueInterRowMin)^2);
        
return;
